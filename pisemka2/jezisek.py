"""
Resenim teto pisemky bude zpracovat 3 dopisy (letter1, letter2 a letter3) od 3 deti, ktere pisou jeziskovi a nakoupit veci z dopisu.
V kazdem dopise je objekt, ktery je mozne koupit na eshopu (promenna stores).
Cilem je nakoupit jeziskovi v eshopech vsechny veci, o ktere si deti napsaly.
Jezisek ma omezeny rozpocet (promenna money), ale muze si vzit uver od banky (promenna bank).
Projdete dopisy deti a najdete v dopisech veci, ktere mate koupit. Veci jsou oddelene - (pomlckou) a jsou vzdy na konci vet.
V dopisech od deti jsou gramaticke chyby, takze bude potreba hledat varianty s i/y nebo ceske prepisy (pro plejstejsn
budete hledat playstation).
Tyto veci se pote pokuste najit v eshopech a "koupit". Pri koupeni musite odecist jednu polozku z poctu dostupnych
polozek na eshopu a odecist penize jeziska.
Preferujte nejlevnejsi moznost.
Pokud jezisek nebude mit na danou vec dost penez, bude si muset pujcit od banky (odectete penize z banky).
Nakonec programu vytvorite formatovany vypis do konzole, ktery bude obsahovat informace o vsech nakoupenych polozkach (obchod, cena).
Dale bude obsahovat kolik jeziskovy zbylo penez a kolik si pujcil od banky a o kolik vic zaplati na urocich.

SHRNUTI:
1. Zpracujete dopisy od deti (zohlednite gramaticke chyby a ceske tvary)
2. Vyhledate darky z dopisu na eshopu
3. Koupite nejlevnejsi darky na eshopech a odectete pocet polozek v obchode
4. Pokud jeziskovi nevyjdou penize tak si pujcite od banky
5. Vypisete informace o nakupech a jeziskovych financich

ZPUSOB IMPLEMENTACE JE NA VAS. SAMOTNE RESENI (FUNKCE) BUDETE MUSET VYMYSLET SAMI.
"""

bank = 1000000
rate = 19.9
money = 30000

letter1 = "Myly jezisku, na vanoce bych si pral -hodinky s vodotriskem"
letter2 = "Na vanoce bich moc chtel -plejstejsn"
letter3 = "Na vanoce bych si prala -ponika"

stores = [
    {'name': 'Alza', 'products': [
        {'name': 'hodinky s vodotryskem', 'price': 150, 'no_items': 5},
        {'name': 'pocitac', 'price': 20000, 'no_items': 2},
        {'name': 'ponik', 'price': 50000, 'no_items': 1}
    ]},
    {'name': 'CZC', 'products': [
        {'name': 'playstation', 'price': 9000, 'no_items': 5},
        {'name': 'Dobroty Ladi Hrusky', 'price': 10, 'no_items': 100},
        {'name': 'ponik', 'price': 70000, 'no_items': 1}
    ]}
]


def grammar_nazi(gift):
    prev = ''
    soft = ['c', 'j']
    hard = ['h', 'k', 'r']
    for i in range(len(gift) - 1):
        char = gift[i]
        if char == 'i' and prev in hard:
            gift = gift[0:i] + 'y' + gift[i + 1:len(gift)]
        if char == 'y' and prev in soft:
            gift = gift[0:i] + 'i' + gift[i + 1:len(gift)]
        prev = char
    return gift


def get_gift(lett):
    rest, gift = lett.split('-')
    gift = grammar_nazi(gift)
    if gift == 'plejstejsn':
        gift = 'playstation'
    if gift == 'ponika':
        gift = 'ponik'
    return gift


def search_shop(gift):
    variants = {}
    for store in stores:
        for product in store['products']:
            if gift in product['name']:
                variants[store['name']] = product['price']
    return variants


def cheaper(variants):
    min = 100000000000
    for key, value in variants.items():
        if value < min:
            cheap = {}
            cheap[key] = value
            min = value
    return cheap


def buy(gift, shop, money):
    for store in stores:
        if store['name'] == shop:
            for product in store['products']:
                if gift in product['name']:
                    product['no_items'] -= 1
                    money -= product['price']
    return money


def loan(money, bank, rate):
    must_pay = money + money * (rate / 100)
    bank += money
    money = 0
    return must_pay


def final_report(gift, shop, price, money, must_pay):
    if must_pay > 0:
        print('Byl koupen darek {} v obchode {} za {} kc.\nJeziskovi zbylo {} kc a musel si pujcit. Dluzi {} kc.'
              .format(gift, shop, price, money, must_pay))
    else:
        print('Byl koupen darek {} v obchode {} za {} kc.\nJeziskovi zbylo {} kc.'.format(gift, shop, price, money))


def main(money):
    gift_list = [letter1, letter2, letter3]
    for letter in gift_list:
        gift = get_gift(letter)
        variants = search_shop(gift)
        if len(variants) > 1:
            variants = cheaper(variants)
        for key in variants.keys():
            shop = key
        price = variants[shop]
        money = buy(gift, shop, money)
        must_pay = 0
        if money < 0:
            must_pay = loan(money, bank, rate) * -1
            money = 0
        final_report(gift, shop, price, money, must_pay)


main(money)