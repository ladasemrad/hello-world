# 1 bod
# z retezce string1 vyberte pouze prvni tri slova a pridejte za ne tri tecky. Vypiste je
# spravny vypis: There are more...
string1 = 'There are more things in Heaven and Earth, Horatio, than are dreamt of in your philosophy.'

words = string1.split(' ')
res = words[0] + ' ' + words[1] + ' ' + words[2] + '...'
print(res)
print()

# 1 bod
# z retezce string1 vyberte pouze poslednich 11 znaku a vypiste je
# spravny vypis: philosophy.

print(string1[-11:-1])
print()


# 1 bod
# retezec string2 rozdelte podle carek a jednotlive casti vypiste
# spravny vypis:
# I wondered if that was how forgiveness budded; not with the fanfare of epiphany
# but with pain gathering its things
# packing up
# and slipping away unannounced in the middle of the night.
string2 = 'I wondered if that was how forgiveness budded; not with the fanfare of epiphany, but with pain gathering its things, packing up, and slipping away unannounced in the middle of the night.'

list2 = string2.split(',')
for elem in list2:
    if elem[0] == ' ':
        print(elem[1:])
    else:
        print(elem)
print()

# 2 body
# v retezci string2 spocitejte cetnost jednotlivych pismen a pote vypiste jednotlive cetnosti i pismenem
# spravny vypis: a: 12
#                b: 2
# atd.

dct = {}
for char in string2:
    if char.isalpha():
        char = char.capitalize()
        if char in dct.keys():
            dct[char] += 1
        else:
            dct[char] = 1
for key in sorted(dct.keys()):
    print('{}: {}'.format(key, dct[key]))
print()

# 5 bodu
# retezec datetime1 predstavuje datum a cas ve formatu YYYYMMDDHHMMSS
# zparsujte dany retezec (rozdelte ho na rok, mesic, den, hodiny, minuty, sekundy a vytvorte z nej datum tak jak definuje
# funkce datetime. Pouzijete knihovnu a tridu datetime (https://docs.python.org/3/library/datetime.html#datetime.datetime)
# Potom slovy odpovezte na otazku: Co je to cas od epochy (UNIX epoch time)?
# Odpoved:
# Nasledne odectete zparsovany datum a cas od aktualniho casu (casu ziskaneho z pocitace pri zavolani jiste funkce)
# a vypocitejte kolik hodin cini rozdil a ten vypiste
d = '20181121191930'

from datetime import datetime

year = int(d[0:4])
month = int(d[4:6])
day = int(d[6:8])
hour = int(d[8:10])
min = int(d[10:12])
sec = int(d[12:14])
iso = datetime(year, month, day, hour, min, sec)
print(iso)
print()



# 5 bodu
# v retezci string3 jsou slova prehazena. V promenne correct_string3 jsou slova spravne.
# vytvorte novou promennou, do ktere poskladate spravne retezec string3 podle retezce correct_string3
# nasledne vypocitej posun mist o ktere muselo byt slovo posunuto, aby bylo dano do spravne pozice.
# nereste pritom smer pohybu
# vypiste spravne poradi vety a jednotliva slova s jejich posunem
# napr. war: 8
#       the: 6
# atd.
string3 = 'war concerned itself with which Ministry The of Peace'
correct_string3 = 'The Ministry of Peace which concerned itself with war'

lst = correct_string3.split(' ')
dct = {}
for i in range(len(lst)):
    dct[i] = lst[i]
count = len(lst)
new_lst = [' ' for j in range(count)]
list_to_make = string3.split(' ')
order = 0
move_dct = {}
for word in list_to_make:
    for key in dct.keys():
        if word == dct[key]:
            new_lst[key] = word
            move_dct[word] = key - order
    order += 1
str_to_make = ''
for word in new_lst:
    str_to_make += word + ' '
print(str_to_make[:-1])
for key, value in move_dct.items():
    if value < 0:
        value = value * -1
    print('{}: {}'.format(key, value))






