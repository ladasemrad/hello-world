# 20 bodu
'''Kdo hral textovou hru od Googlu tak bude v obraze. Cilem bude udelat mapu. Muzete si udelat vlastni nebo pouzit moji.
Mapa se bude skládat z několika částí a bude reprezentována textem. Obrázek mapy se nachází pod tímto textem
Cilem toho cviceni bude spise vymyslet jak toto cviceni vyresit. Samotna implementace nebude tak narocna.

Prace s dvourozmernym polem je seznamem (polem) je jednoducha

pole = [['a','b','c','d'],
        ['e','f','g','h'],
        ['i','j','k','l']]

Dvourozmerne pole obsahuje radky a sloupce. Kazda hodnota ma svuj jedinecny index definovany indexem radku a sloupce
napr. pole[0][1] vybere hodnotu na prvni radku ve druhem sloupci 'b'.
      pole[1][3] vybere hodnotu na druhem radku ve ctvrtem sloupci 'h'.
Vice informaci najdete na internetu: https://www.tutorialspoint.com/python/python_2darray.htm

Mapa (dvourozmerne pole) predstavuje bludiste. Vase postava se na zacatku hry bude nachazet na pozici pismene S (index [7][0]).
Cílem je dostat se do cile, ktery oznacuje pismeno F (index [0][9]).
Budete se ptat uzivatele stejne jako v textove hre na informaci o tom kterym smerem chce jit - nahoru, doprava, dolu, doleva.
Po každém zadání posunu vykreslíte mapu, kde bude zobrazena pozice postavicky v mape a pozici v jejim primem okoli. Bude to vypadat asi takto:

      ?????
      ?***?
      ?*P.?
      ?*.*?
      ?????

Tečky představují cestu (místo kam může postavička stoupnout). Hvězdičky představují zeď, tam postavička nemůže vstoupit.
Ostatní okolí postavy (zbytek mapy) bude ukryt postavě a budou ho představovat otazníky.

# BONUS: 5 bodů
# Udělejte verzi, kde se bude ukládat a při vykreslování mapy zobrazovat i místo, které postava už prošla a tudíž ho zná.
'''

game_map = [['*','*','*','*','*','*','.','.','.','F'],
            ['*','*','*','*','.','.','.','*','*','*'],
            ['.','.','.','.','.','*','*','*','*','*'],
            ['.','*','*','*','*','*','*','*','.','.'],
            ['.','.','.','*','*','.','.','.','.','.'],
            ['*','*','.','.','.','.','*','*','*','*'],
            ['*','*','.','*','*','*','*','*','*','*'],
            ['S','.','.','.','.','.','.','.','.','.']]


def print_state(x, y, game_map, printed):

    for row in range(len(printed)):
        for col in range(len(printed[0])):
            if row <= x + 1 and row >= x - 1 and col <= y + 1 and col >= y - 1:
                printed[row][col] = game_map[row][col]
    printed[0][9] = 'F'
    printed[7][0] = 'S'
    printed[x][y] = 'P'
    for row in printed:
        for char in row:
            print(char, end=' ')
        print()


def find_place(dir, x, y, game_map):
    if dir == 'N':
        x2 = x - 1
        y2 = y
    elif dir == 'S':
        x2 = x + 1
        y2 = y
    elif dir == 'W':
        x2 = x
        y2 = y - 1
    elif dir == 'E':
        x2 = x
        y2 = y + 1
    if x2 > len(game_map) - 1 or x2 < 0 or y2 > len(game_map[0]) - 1 or y2 < 0:
        print('Out of labyrinth!')
        return x, y
    elif game_map[x2][y2] == '*':
        print('You hit the wall!')
        return x, y
    else:
        return x2, y2


def step(x, y, game_map):
    print('Which direction would you like to go?')
    dir = str(input('Use points of the compass: N, E, W, S: '))
    if dir == 'N' or dir == 'W' or dir == 'E' or dir == 'S':
        x, y = find_place(dir, x, y, game_map)
        return x, y
    else:
        print('Unsuccessful try')
        return x, y


def main(game_map):
    print('Welcome in labyrinth. Your goal is to move from lower-left corner to the upper-right corner of labyrinth')
    x = len(game_map) - 1
    y = 0
    printed = [['?' for j in range(10)] for i in range(8)]
    while x != 0 or y != len(game_map[0]) - 1:
        print_state(x, y, game_map, printed)
        x, y = step(x, y, game_map)
    print('You win!')


main(game_map)