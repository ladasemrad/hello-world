# v tomto ukolu budete simulovat seznam zvirat v ZOO
# seznam zvirat, ktera zoo vlastni bude predstavovat seznam (list)
animals = []

# jednotliva zvirata budou reprezentovana slovnikem, ktery bude mit jasne danou strukturu:
#
# species: druh zvirete
# price: cena zvirete
# sex: pohlavi zvirete (bude nabyvat jen hodnot 'male' a 'female'
# illness: informace jestli je zvire nemocne (bude nabyvat jen hodnot True nebo False)
# hunger: informace o tom jak velky ma zvire hlad (bude nabyvat 3 hodnot - 'starvation', 'fed', 'surfeit')
#
# doplhin = {'species': 'dolphin', 'price': 40000, 'sex': 'male', 'illness': True, 'hunger': 'fed'}
#
# !!!POZOR!!!
# v ramci zjednoduseni se v ZOO nebude nachazet vice zvirat od jednoho druhu, tudiz se v ZOO
# napriklad nemuzou nachazet dva delfini. Vzdy tam bude maximalne jeden. Neni to nijak osetreno, ale volani funkci
# na konci testu tam nebude pridavat nikdy stejny druh.
#
# Dopiste funkce (nahradte pass vasim kodem funkce). Neupravujte parametry funkci ani volani funkci dole.


def add_animal(animal):
    """
    (3b)
    Funkce, ktera prida do ZOO (seznamu zvirat) zvire, jen pokud není nemocné (illness == False).
    Pri uspesnem pridani vrati True, pri neuspesnem (pokud je zvire nemocne) False.
    :param (dict) animal: zvire, ktere budete pridavat
    :return (bool): vraci True, kdyz zvire bylo pridano do ZOO. Jinak vraci False
    """
    if animal['illness'] is False:
        animals.append(animal)
        return True
    return False


def add_animals(*args):
    """
    (3b)
    Funkce prida do ZOO vice zvirat (pouze kdyz nejsou nemocne), ktere budou zadany jako jednotlive parametry funkce
    :param (dicts) args: vice zvirat zadanych jako jednotlive parametry
    :return (bool): vrati True pokud se vsechny zvirata podari pridat. Pokud ne, tak vrati False
    """
    for arg in args:
        if add_animal(arg) is False:
            return False
    return True


def remove_animal(species):
    """
    (3b)
    Funkce, ktera odstrani ze ZOO zvire pokud se v ni zvire nachazelo
    :param (str) species: druh zvirete, ktere budete odstranovat
    :return (bool): vraci True, kdyz bylo zvire odstraneno ze ZOO. Jinak vraci False
    """
    for beast in animals:
        if beast['species'] == species:
            animals.remove(beast)
            return True
    return False


def get_price():
    """
    (1b)
    Funkce, ktera vrati sumu cen vsech zvirat v ZOO
    :return (int): suma cen vsech zvirat v ZOO
    """
    suma = 0
    for beast in animals:
        suma += beast['price']
    return suma


def get_males():
    """
    (2b)
    Funkce, ktera vrati pocet samcu v ZOO
    :return (int): pocet samcu v ZOO
    """
    count = 0
    for beast in animals:
        if beast['sex'] == 'male':
            count += 1
    return count


def get_females():
    """
    (2b)
    Funkce, ktera vrati pocet samic v ZOO
    :return (int): pocet samic v ZOO
    """
    return int(len(animals) - get_males())


def feed_animal(species):
    """
    (3b)
    Funkce, ktera nakrmí zvíře (zvedne hodnotu hunger). Pokud bude puvodni hodnota zvirete na starvation - zvedne
    hodnotu na fed. Pokud bude hodnota na fed - zvedne hodnotu na surfeit. Pokud bude hodnota na surfeit nestane se nic,
    popripade nastavi znovu surfeit
    starvation -> fed
    fed -> surfeit
    surfeit -> surfeit
    :param (str) species: druh zvirete, ktere budete krmit
    :return (int): pocet samcu v ZOO
    """
    for beast in animals:
        if beast['species'] == species:
            if beast['hunger'] == 'starvation':
                beast['hunger'] = 'fed'
            elif beast['hunger'] == 'fed':
                beast['hunger'] = 'surfeit'
            return True
    return False


def heal_animal(species):
    """
    (3b)
    Funkce, ktera vyleci zvire pokud je nemocne (illness == True). Pokud dane zvire vyleci vrati funkce True. Jinak False
    :param (str) species:
    :return (bool): vrati True pokud bylo zvire vyleceno. Pokud ne, tak vrati False
    """
    for beast in animals:
        if beast['species'] == species:
            if beast['illness']:
                beast['illness'] = False
                return True
    return False


from random import randint
# BONUS


def profit_per_day():
    """
    (3b)
    Funkce, ktera bude simulovat pocet navstevniku a vysi zisku ZOO. Pomoci knihovny random
    https://stackoverflow.com/questions/3996904/generate-random-integers-between-0-and-9
    a funkce randint() vygenerujte nejdrive pocet navstevniku za den v rozmezi od 1000 do 10 000.
    Pote pro kazdeho navstevnika generujte castku, kterou v ZOO nechal (v rozmezi 300-2500).
    Odectete naklady za provoz za den (jsou fixni - 100 000) a vratte zisk ZOO.
    :return (int): zisk ZOO za den
    """
    visit = randint(1000, 10000)
    paid_total = 0
    for i in range(visit + 1):
        paid = randint(300, 2500)
        paid_total += paid
    return int(paid_total - 100000)


# Pokud budete chtit testovat jednotlive funkce muzete si je zavolat sami nebo muzete vyuzit volani zakomentovanych funkci nize
# Volani funkci nize neupravujte. Nakonci je odkomentujte (vymazte ''' '''). Podle vypisu poznate spravnost implementace.


# SPRAVNY VYPIS BY MEL VYPADAT TAKTO
# Pridani delfina: False
# Pridani tygra: True
# Pridani vice zvirat: True
# Odstran slona: True
# Odstran vlka: False
# Celkova cena zvirat: 850000
# Celkovy pocet samcu: 2
# Celkovy pocet samic: 2
# Nakrm vlka: False
# Nakrm pandu True
# Uzdrav delfina: False
# Uzdrav lva: False


print('Pridani delfina:', add_animal({'species': 'dolphin', 'price': 40000, 'sex': 'male', 'illness': True, 'hunger': 'fed'}))
print('Pridani tygra:', add_animal({'species': 'tiger', 'price': 100000, 'sex': 'female', 'illness': False, 'hunger': 'starvation'}))

print('Pridani vice zvirat:', add_animals({'species': 'lion', 'price': 150000, 'sex': 'male', 'illness': False, 'hunger': 'surfeit'},
                                          {'species': 'panda', 'price': 400000, 'sex': 'female', 'illness': False, 'hunger': 'fed'},
                                          {'species': 'elephant', 'price': 300000, 'sex': 'male', 'illness': False, 'hunger': 'starvation'},
                                          {'species': 'cheetah', 'price': 200000, 'sex': 'male', 'illness': False, 'hunger': 'fed'}))

print('Odstran slona:', remove_animal('elephant'))
print('Odstran vlka:', remove_animal('wolf'))

print('Celkova cena zvirat:', get_price())
print('Celkovy pocet samcu:', get_males())
print('Celkovy pocet samic:', get_females())

print('Nakrm vlka:', feed_animal('wolf'))
print('Nakrm pandu', feed_animal('panda'))

print('Uzdrav delfina:', heal_animal('dolphin'))
print('Uzdrav lva:', heal_animal('lion'))

print('Zisk za den:', profit_per_day())
